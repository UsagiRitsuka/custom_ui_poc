package poc.wachiwu.flexibleheaderbanner;

/**
 * Created by wachiwu on 2018/4/11.
 */
import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.os.Build;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.ViewPropertyAnimatorListener;
import android.support.v4.view.animation.FastOutSlowInInterpolator;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.Interpolator;

public class FabAnimationUtils {

    private static final long DEFAULT_DURATION = 300L;
    private static final Interpolator FAST_OUT_SLOW_IN_INTERPOLATOR = new FastOutSlowInInterpolator();

    public static void scaleIn(final View fab) {
        scaleIn(fab, DEFAULT_DURATION, null);
    }

    public static void scaleIn(final View fab, long duration, final ScaleCallback callback) {
        fab.setVisibility(View.VISIBLE);
        ViewCompat.animate(fab)
            .scaleX(1.0F)
            .scaleY(1.0F)
            .alpha(1.0F)
            .translationX(-400)
            .translationY(-100)
            .scaleX(5)
            .setDuration(duration)
            .setInterpolator(FAST_OUT_SLOW_IN_INTERPOLATOR)
            .withLayer()
            .setListener(new ViewPropertyAnimatorListener() {
                public void onAnimationStart(View view) {
                    if (callback != null) callback.onAnimationStart();
                }

                public void onAnimationCancel(View view) {
                }

                public void onAnimationEnd(View view) {
                    view.setVisibility(View.VISIBLE);
                    if (callback != null) callback.onAnimationEnd();
                }
            }).start();

    }

    public static void scaleOut(final View fab) {
        scaleOut(fab, DEFAULT_DURATION, null);
    }

    public static void scaleOut(final View fab, final ScaleCallback callback) {
        scaleOut(fab, DEFAULT_DURATION, callback);
    }

    public static void scaleOut(final View fab, long duration, final ScaleCallback callback) {
        ViewCompat.animate(fab)
            .scaleX(0.0F)
            .scaleY(0.0F).alpha(0.0F)
            .setInterpolator(FAST_OUT_SLOW_IN_INTERPOLATOR)
            .setDuration(duration)
            .withLayer()
            .setListener(new ViewPropertyAnimatorListener() {
                public void onAnimationStart(View view) {
                    if (callback != null) callback.onAnimationStart();
                }

                public void onAnimationCancel(View view) {
                }

                public void onAnimationEnd(View view) {
                    view.setVisibility(View.VISIBLE);
                    if (callback != null) callback.onAnimationEnd();
                }
            }).start();
    }


    public static void expend(final View fab, long duration, final ScaleCallback callback) {
        fab.setVisibility(View.VISIBLE);
//        final int height = fab.getLayoutParams().height * 200;
//        ViewGroup.LayoutParams lp = fab.getLayoutParams();
//        lp.height = height;
//        fab.setLayoutParams(lp);
        Animation animation = AnimationUtils.loadAnimation(fab.getContext(), R.anim.expand);
        fab.setAnimation(animation);
        animation.start();

//        ObjectAnimator animator = ObjectAnimator.ofFloat(fab, "scaleY", lp.height * 2)
//                .setDuration(1000);
//        animator.addListener(new Animator.AnimatorListener() {
//            @Override
//            public void onAnimationStart(Animator animation) {
//
//            }
//
//            @Override
//            public void onAnimationEnd(Animator animation) {
//
//            }
//
//            @Override
//            public void onAnimationCancel(Animator animation) {
//
//            }
//
//            @Override
//            public void onAnimationRepeat(Animator animation) {
//
//            }
//        });
//        animator.start();

//        AnimatorSet aniSet = new AnimatorSet();
//        aniSet.setDuration(4000);
//        aniSet.setInterpolator(new BounceInterpolator());// 弹跳效果的插值器
//        aniSet.playTogether(animator0,
//                animator1,
//                animator2,
//                animator3,
//                animator4);// 同时启动5个动画
//        aniSet.start();

    }

    public static void collapse(final View fab, long duration, final ScaleCallback callback) {
        fab.setVisibility(View.VISIBLE);
        ViewCompat.animate(fab)
                .scaleY(0.0F)
                .scaleXBy(0)
                .alpha(0.0F)
                .setInterpolator(FAST_OUT_SLOW_IN_INTERPOLATOR)
                .setDuration(duration)
                .withLayer()
                .setListener(new ViewPropertyAnimatorListener() {
                    public void onAnimationStart(View view) {
                        if (callback != null) callback.onAnimationStart();
                    }

                    public void onAnimationCancel(View view) {
                    }

                    public void onAnimationEnd(View view) {
                        view.setVisibility(View.INVISIBLE);
                        if (callback != null) callback.onAnimationEnd();
                    }
                }).start();
    }

    public interface ScaleCallback {
        public void onAnimationStart();

        public void onAnimationEnd();
    }


}