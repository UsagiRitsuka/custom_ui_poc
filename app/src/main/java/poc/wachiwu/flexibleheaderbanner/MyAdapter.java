package poc.wachiwu.flexibleheaderbanner;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.graphics.Point;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;


/**
 * Created by wachiwu on 2018/3/7.
 */

public class MyAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{
    private LayoutInflater inflater;
    private final String[] dataList = {"AA", "BB", "CC", "DD", "EE", "FF", "GG", "HH", "II", "JJ"};
    private final int ITEM_TYPE = 1;
    private Context context;
    private ViewGroup lastExpandLayout = null;
    private OnRecyclerViewListener onRecyclerViewListener;
    private boolean isLock = false;
    private boolean isUnlockByExpand = true;
    public MyAdapter(Context context){
        this.context = context;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder = ITEM_TYPE == 0 ?
            new ViewHolder(inflater.inflate(R.layout.item_layout, null)) :
            new GroupViewHolder(inflater.inflate(R.layout.item_group, null));

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if(ITEM_TYPE == 0) {
            ((ViewHolder)holder).tv.setText(dataList[position]);
        } else if(ITEM_TYPE == 1){
            GroupViewHolder groupViewHolder = (GroupViewHolder)holder;
            int startIndex = ++position * 3 - 3;
            int endIndex = startIndex + 2;

            for(int i = startIndex; i <= endIndex; i++){
                if(i < dataList.length){
                    switch (i % 3) {
                        case 0:
                            groupViewHolder.tv1.setText(dataList[i]);
                            break;
                        case 1:
                            groupViewHolder.tv2.setText(dataList[i]);
                            break;
                        case 2:
                            groupViewHolder.tv3.setText(dataList[position]);
                            break;
                    }
                }
            }

            ViewGroup.LayoutParams lp = groupViewHolder.subArea.getLayoutParams();
            final int collapseHeight = lp.height;
            final int expandHeight = lp.height * 150;
            groupViewHolder.root.setOnClickListener(view -> {
                if(!isLock) {
                    isLock = true;
                    ViewGroup.LayoutParams subAreaLP = groupViewHolder.subArea.getLayoutParams();
                    if (subAreaLP.height <= collapseHeight) {
                        isUnlockByExpand = true;
                        if (lastExpandLayout != null) {
                            collapse(lastExpandLayout, collapseHeight);
                        }
                        lastExpandLayout = groupViewHolder.subArea;
                        expand(groupViewHolder.subArea, expandHeight);
                    } else {
                        isUnlockByExpand = false;
                        lastExpandLayout = null;
                        collapse(groupViewHolder.subArea, collapseHeight);
                    }
                }
            });
        }
    }

    private void expand(ViewGroup viewGroup, int height){
        Runnable expand = new Runnable() {
            @Override
            public void run() {
                ViewGroup.LayoutParams lp = viewGroup.getLayoutParams();
                if(lp.height < height - 40){
                    lp.height += 40;
                    viewGroup.setLayoutParams(lp);
                    ThreadManager.getInstance().postToUIThread(this, 10);
                } else{
                    lp.height = height;
                    viewGroup.setLayoutParams(lp);

                    int[] location = new int[2];
                    viewGroup.getLocationOnScreen(location);
                    Log.v("WACHI", "y position: " + location[1] + "  screenHeight: " + MainActivity.screenHeight);
                    int dy = (location[1] + height) - MainActivity.screenHeight;
                    if(dy > 0){
                        onRecyclerViewListener.scrollToPosition(dy);
                    }

                    if(isUnlockByExpand) {
                        isLock = false;
                    }
                }
            }
        };

        ThreadManager.getInstance().postToUIThread(expand);
    }

    private void collapse(ViewGroup viewGroup, int height){
        Runnable collapse = new Runnable() {
            @Override
            public void run() {
                ViewGroup.LayoutParams lp = viewGroup.getLayoutParams();
                if(lp.height > height + 40){
                    lp.height -= 40;
                    viewGroup.setLayoutParams(lp);
                    ThreadManager.getInstance().postToUIThread(this, 10);
                } else {
                    lp.height = height;
                    viewGroup.setLayoutParams(lp);
                    int[] location = new int[2];
                    viewGroup.getLocationOnScreen(location);
                    Log.v("WACHI", "y position: " + location[1]);

                    if(!isUnlockByExpand) {
                        isLock = false;
                    }
                }
            }
        };

        ThreadManager.getInstance().postToUIThread(collapse);
    }

    @Override
    public int getItemCount() {
        return ITEM_TYPE == 0 ? dataList.length : (dataList.length / 3 + (dataList.length % 3 == 0 ? 0 : 1));
    }

    @Override
    public int getItemViewType(int position) {
        return ITEM_TYPE;
    }

    class ViewHolder extends RecyclerView.ViewHolder{
        private TextView tv;
        public ViewHolder(View view){
            super(view);
            tv = view.findViewById(R.id.text);
        }
    }

    class GroupViewHolder extends RecyclerView.ViewHolder{
        private ViewGroup root;
        private TextView tv1;
        private TextView tv2;
        private TextView tv3;
        private ViewGroup subArea;

        public GroupViewHolder(View view){
            super(view);
            root = view.findViewById(R.id.root);
            tv1 = view.findViewById(R.id.item_1).findViewById(R.id.text);
            tv2 = view.findViewById(R.id.item_2).findViewById(R.id.text);
            tv3 = view.findViewById(R.id.item_3).findViewById(R.id.text);
            subArea = view.findViewById(R.id.sub);
        }
    }

    public void setOnRecyclerViewListener(OnRecyclerViewListener onRecyclerViewListener) {
        this.onRecyclerViewListener = onRecyclerViewListener;
    }

    public interface OnRecyclerViewListener{
        void scrollToPosition(int position);
    }
}
