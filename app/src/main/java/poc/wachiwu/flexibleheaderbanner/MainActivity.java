package poc.wachiwu.flexibleheaderbanner;

import android.app.Dialog;
import android.content.Intent;
import android.os.Message;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import java.lang.ref.WeakReference;

import poc.wachiwu.flexibleframe.FlexibleLayout;


public class MainActivity extends AppCompatActivity implements MyAdapter.OnRecyclerViewListener {
    private RecyclerView recyclerView;
    private FlexibleLayout flexibleLayout;
    private int range = 300;
    private int minHeigh = 100;
    private int maxHeight = -1;
    public static int screenHeight = 0;

    public static final String PACKAGE_NAME = "jp.naver.line.android";
    public static final String CLASS_NAME = "jp.naver.line.android.activity.selectchat.SelectChatActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        findView();
        setupViewComponent();

        MyAdapter adapter = new MyAdapter(this);
        adapter.setOnRecyclerViewListener(this);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.v("WACHI", "h: " + flexibleLayout.getLayoutParams().height);
        if(maxHeight == -1) {
            DisplayMetrics dm = new DisplayMetrics();
            getWindowManager().getDefaultDisplay().getMetrics(dm);
            screenHeight = dm.heightPixels;
            maxHeight = (int)(dm.heightPixels * 0.3);
            ViewGroup.LayoutParams lp = flexibleLayout.getLayoutParams();
            lp.height = maxHeight;
            flexibleLayout.setLayoutParams(lp);
        }
    }

    private void findView(){
        flexibleLayout = findViewById(R.id.flexible_frame);
        recyclerView = findViewById(R.id.recycler_view);
        recyclerView.scrollTo(0, 1);
    }

    private void setupViewComponent(){
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int scrollOffset = recyclerView.computeVerticalScrollOffset();
                Log.v("WACHI", "scroll offset" + scrollOffset);
//                adjustFlexibleLayout(scrollOffset);
            }
        });

        flexibleLayout.setOnClickListener(view -> {
//            onShareClick();
        });

    }

    private void adjustFlexibleLayout(int scrollOffset){
        if (scrollOffset < range) {
            ViewGroup.LayoutParams lp = flexibleLayout.getLayoutParams();
            double ratio = (double)scrollOffset / (double)maxHeight;
            double diff = (maxHeight - minHeigh) * ratio;

            lp.height = maxHeight - (int)diff;
            flexibleLayout.setLayoutParams(lp);

//            flexibleLayout.setOffset(-(int)(diff / 2), lp.height - (int)(diff/2));
//            flexibleLayout.invalidate(-(int)(diff / 2), 0, lp.width, lp.height - (int)(diff/2));
//            flexibleLayout.layout(0, -(int)(diff / 2), lp.width, lp.height - (int)(diff/2));



            Log.v("WACHI", "lp.height : " + lp.height );
        }
    }

    public void onShareClick() {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setClassName(PACKAGE_NAME, CLASS_NAME);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_TEXT, "Hello World!!");
        startActivity(intent);
    }

    @Override
    public void scrollToPosition(int position) {
        recyclerView.smoothScrollBy(0, position);
    }
}
