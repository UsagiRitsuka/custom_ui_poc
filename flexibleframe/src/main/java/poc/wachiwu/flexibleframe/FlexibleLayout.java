package poc.wachiwu.flexibleframe;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.FrameLayout;

/**
 * Created by wachiwu on 2018/3/7.
 */

public class FlexibleLayout extends FrameLayout{
    final private String TAG = FlexibleLayout.class.getSimpleName();
    private int top = 0;
    private int bottom = 0;

    public FlexibleLayout(Context context) {
        super(context);
    }

    public FlexibleLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public FlexibleLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        Log.v(TAG, "onLayout: " + changed + "  " + l + "  " + t + "  " + r + "  " + b);
        super.onLayout(changed, l, t, r, b);
    }

    public void setOffset(int top, int bottom){
        this.top = top;
        this.bottom = bottom;
    }
}
